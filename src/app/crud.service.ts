import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

const stagingUrl = 'http://staging.cmedhealth.com/oauth/token';
const baseUrl  = stagingUrl;

@Injectable({
  providedIn: 'root'
})
export class CrudService {
  constructor(private axios: HttpClient) { }

  getPosts() {
    return this.axios.get('https://my-json-server.typicode.com/typicode/demo/posts');
  }
  deletePost(id: number) {
    return this.axios.delete('https://jsonplaceholder.typicode.com/posts/' + id);
  }
  getUsers() {
    return this.axios.get('https://jsonplaceholder.typicode.com/users/');
  }
  login(credential: any) {
    return this.axios.get(baseUrl, {params: credential});
  }
}

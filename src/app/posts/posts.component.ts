import { Component, OnInit } from '@angular/core';
import {CrudService} from '../crud.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts: any = [];

  constructor(private apiService: CrudService) { }
  ngOnInit() {
    this.getPosts();
  }

  private getPosts() {
    this.apiService.getPosts().subscribe(posts => this.posts = posts);
  }
  deletePost(id: number) {
    this.apiService.deletePost(id).subscribe((result) => {
      console.log(result);
    }, (error) => {
      console.log('error' + error);
    });
  }
}

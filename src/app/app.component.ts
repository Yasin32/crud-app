import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isLoggedIn: boolean;
  loginInfo: any;
  constructor(private router: Router) {}
  ngOnInit() {
    this.checkLogin();
  }

  loggedOut() {
    this.isLoggedIn = false;
    localStorage.removeItem('loginInfo');
    this.router.navigate(['/login']);
  }

  checkLogin() {
    this.loginInfo = JSON.parse(localStorage.getItem('loginInfo'));
    if (this.loginInfo && this.loginInfo.expires_in > 0 ) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
  }

}

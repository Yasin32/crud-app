import { Component, OnInit } from '@angular/core';
import {CrudService} from '../crud.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: any = [];

  constructor(private service: CrudService) { }

  ngOnInit() {
    this.getUsers();
  }

  private getUsers() {
    this.service.getUsers().subscribe(users => this.users = users);
  }
}

import { Component, OnInit } from '@angular/core';
import {CrudService} from '../crud.service';
import {Router} from '@angular/router';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  credential = {
    username: '',
    password : '',
    grant_type : 'password',
    client_id : 'android-client',
    client_secret : 'android-secret',
  };
  fromLocal: any;
  constructor(private apiService: CrudService, private router: Router) { }

  ngOnInit() {
  }

  userLogin() {
    this.apiService.login(this.credential).subscribe((response) => {
      localStorage.setItem('loginInfo', JSON.stringify(response));
      this.fromLocal = JSON.parse(localStorage.getItem('loginInfo'));
      this.router.navigate(['/post']);
    }, (err) => {
      console.log(err);
      alert('User id and password combination is not correct');
    });
  }

}
